package com.bijenkorf.demo.log;

import javax.persistence.*;

@Entity
@Table(schema = "BJK", name = "LOG_TABLE")
public class LogTable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "LOG_ID")
    private Long logId;
    @Column(name = "TIMESTAMP")
    private String timestamp;
    @Column(name = "LEVEL")
    private String level;
    @Column(name = "MESSAGE")
    private String message;

    public LogTable(String timestamp, String level, String message) {
        this.timestamp = timestamp;
        this.level = level;
        this.message = message;
    }

    public Long getLogId() {
        return logId;
    }

    public void setLogId(Long logId) {
        this.logId = logId;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
