package com.bijenkorf.demo.utilities;


public class ImageUtilities {

    public static byte[] optimizer(byte[] input, String imageType){
        //TODO Use imageType to retrieve properties from ImageTypeEnum
        return input;
    }



    public static boolean checkStyleAvailability(String inputType) {
        for (ImageTypeEnum imageType : ImageTypeEnum.values()) {
            if (imageType.getName().equalsIgnoreCase(inputType)) {
                return true;
            }
        }
        return false;
    }
}
