package com.bijenkorf.demo.utilities;

public enum ImageTypeEnum {
    THUMBNAIL("thumbnail", 100,100,100, "skew", "#17202A", "PDF","source1"),
    STYLE1("style1", 50,50,50, "crop", "#17203A", "PDF","source2"),
    STYLE2("style2", 80,40,70, "fill", "#17204A", "JPG","source3");

    private final String name;
    private final int height;
    private final int width;
    private final int quality;
    private final String scaleType;
    private final String fillColor;
    private final String type;
    private final String sourceName;

    ImageTypeEnum(String name, int height, int width, int quality, String scaleType, String fillColor, String type, String sourceName) {
        this.name = name;
        this.height = height;
        this.width = width;
        this.quality = quality;
        this.scaleType = scaleType;
        this.fillColor = fillColor;
        this.type = type;
        this.sourceName = sourceName;
    }

    public String getName() {
        return name;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public int getQuality() {
        return quality;
    }

    public String getScaleType() {
        return scaleType;
    }

    public String getFillColor() {
        return fillColor;
    }

    public String getType() {
        return type;
    }

    public String getSourceName() {
        return sourceName;
    }
}
