package com.bijenkorf.demo.utilities;

public class Costants {
	
	public static final String ORIGINAL_IMAGE_TYPE = "original";

	public static final String LOG_INFO = "INFO";
	public static final String LOG_WARNING = "WARNING";
	public static final String LOG_ERROR = "ERROR";

}
