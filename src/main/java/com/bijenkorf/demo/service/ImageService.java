package com.bijenkorf.demo.service;

import com.bijenkorf.demo.log.LogTable;
import com.bijenkorf.demo.log.LogTableRepository;
import com.bijenkorf.demo.utilities.Costants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.LocalDateTime;

@Service
public class ImageService {

    @Value("${imageSource.endpoint}")
    private String sourceEndpoint;



    @Autowired
    private LogTableRepository logRepo;

    private static Logger logger = LoggerFactory.getLogger(ImageService.class);



    public byte[] retrieveOriginalImage(String fileName) {
        int httpStatusCode = 200;

        String address = this.sourceEndpoint + fileName;
        try {
            httpStatusCode = checkHttpStatuCode(this.sourceEndpoint + fileName);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            logRepo.save(new LogTable(LocalDateTime.now().toString(), Costants.LOG_ERROR, e.getMessage()));
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        if (httpStatusCode == 200) {
            try {
                URL url = new URL(address);
                BufferedImage bImage = ImageIO.read(url);
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                ImageIO.write(bImage, "jpg", bos);
                return bos.toByteArray();
            } catch(Exception e){
                logger.error(e.getMessage(), e);
                logRepo.save(new LogTable(LocalDateTime.now().toString(), Costants.LOG_ERROR, e.getMessage()));
                throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else if (httpStatusCode == 404){
            String message = "Resorce " + address + " not found!";
            logger.info(message);
            logRepo.save(new LogTable(LocalDateTime.now().toString(), Costants.LOG_INFO, message));
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        } else {
            String message = "An error occured in the attempt to retrieve the image from " + address;
            logger.error(message);
            logRepo.save(new LogTable(LocalDateTime.now().toString(), Costants.LOG_ERROR, message));
            throw new ResponseStatusException(HttpStatus.valueOf(httpStatusCode));
        }


    }


    private int checkHttpStatuCode(String urlString) throws Exception {
        URL url = new URL(urlString);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("HEAD");
        int code = conn.getResponseCode();
        conn.disconnect();
        return code;
    }
}
