package com.bijenkorf.demo.service;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.bijenkorf.demo.log.LogTable;
import com.bijenkorf.demo.log.LogTableRepository;
import com.bijenkorf.demo.utilities.Costants;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.time.LocalDateTime;

@Service
public class AwsImageService {

    @Value("${awsProperties.endpoint}")
    private String awsEndpoint;
    @Value("${awsProperties.accessKey}")
    private String awsAccessKey;
    @Value("${awsProperties.secretKey}")
    private String awsSecretKey;
    @Value("${awsProperties.bucketName}")
    private String awsBucketName;



    @Autowired
    private LogTableRepository logRepo;

    private static final Logger logger = LoggerFactory.getLogger(AwsImageService.class);



    public byte[] retrieveImage(String typeName, String fileName){
        String keyName = createS3DirectoryPath(typeName, fileName);

        try {
            AmazonS3 s3Client = s3ClienBuilder();

            S3Object s3object = s3Client.getObject(new GetObjectRequest(this.awsBucketName, keyName));
            InputStream input = s3object.getObjectContent();
            return IOUtils.toByteArray(input);

        } catch (AmazonServiceException ase) {
            if(ase.getErrorCode().equals("NoSuchKey")){
                return new byte[0];
            } else {
                logger.error(ase.getMessage(), ase);
                logRepo.save(new LogTable(LocalDateTime.now().toString(), Costants.LOG_ERROR, ase.getMessage()));
                throw new ResponseStatusException(HttpStatus.NOT_FOUND);
            }
        } catch(Exception e) {
            logger.error(e.getMessage(), e);
            logRepo.save(new LogTable(LocalDateTime.now().toString(), Costants.LOG_ERROR, e.getMessage()));
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }



    public void saveImage(String typeName, String fileName, byte[] image) {
        String keyName = createS3DirectoryPath(typeName, fileName);
        File file = new File(fileName);

        try (FileOutputStream fos = new FileOutputStream(file)){
            fos.write(image);
        } catch (Exception e){
            logger.error(e.getMessage(), e);
            logRepo.save(new LogTable(LocalDateTime.now().toString(), Costants.LOG_ERROR, e.getMessage()));
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        boolean firstFailed = false;
        try {
            AmazonS3 s3Client = s3ClienBuilder();
            s3Client.putObject(this.awsBucketName, keyName, file);
        }  catch (Exception e){
            logger.warn(e.getMessage(), e);
            logRepo.save(new LogTable(LocalDateTime.now().toString(), Costants.LOG_WARNING, e.getMessage()));
            firstFailed = true;
        }

        if(firstFailed){
            try{
                Thread.sleep(200);

                AmazonS3 s3Client = s3ClienBuilder();
                s3Client.putObject(this.awsBucketName, keyName, file);
            } catch (Exception e){
                logger.error(e.getMessage(), e);
                logRepo.save(new LogTable(LocalDateTime.now().toString(), Costants.LOG_ERROR, e.getMessage()));
                throw new ResponseStatusException(HttpStatus.NOT_FOUND);
            }
        }
    }



    public void flushImage(String typeName, String fileName){
        String keyName = createS3DirectoryPath(typeName, fileName);

        try {
            AmazonS3 s3Client = s3ClienBuilder();
            s3Client.deleteObject(this.awsBucketName, keyName);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            logRepo.save(new LogTable(LocalDateTime.now().toString(), Costants.LOG_ERROR, e.getMessage()));
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }



    private AmazonS3 s3ClienBuilder(){
        try {
            AWSCredentials credentials = new BasicAWSCredentials(this.awsAccessKey, this.awsSecretKey);
            return AmazonS3ClientBuilder
                    .standard()
                    .withCredentials(new AWSStaticCredentialsProvider(credentials))
                    .build();

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            logRepo.save(new LogTable(LocalDateTime.now().toString(), Costants.LOG_ERROR, e.getMessage()));
            throw new ResponseStatusException(HttpStatus.valueOf(500));
        }
    }



    private String createS3DirectoryPath(String typeName, String imageFileName){

        StringBuilder stringBuilder = new StringBuilder(typeName).append("/");

        String correctImageFileName = imageFileName.replace("/","_");
        int lenght = correctImageFileName.length() - 4; //TODO To improve
        if(lenght > 4 && lenght < 9){
            stringBuilder.append(correctImageFileName.substring(0,4)).append("/");
        } else if (lenght >= 9){
            stringBuilder.append(correctImageFileName.substring(0,4)).append("/")
                    .append(correctImageFileName.substring(4,8)).append("/");
        }
        stringBuilder.append(correctImageFileName);

        return stringBuilder.toString();
    }

}
