package com.bijenkorf.demo.command;

import com.bijenkorf.demo.dto.ImageDto;
import com.bijenkorf.demo.log.LogTable;
import com.bijenkorf.demo.log.LogTableRepository;
import com.bijenkorf.demo.utilities.Costants;
import com.bijenkorf.demo.utilities.ImageTypeEnum;
import com.bijenkorf.demo.utilities.ImageUtilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDateTime;


@Component
@Scope("prototype")
public class FlushImageCommand {

    private ImageDto dto;

    public FlushImageCommand(ImageDto dto) {
        this.dto = dto;
    }



    @Autowired
    private AwsImageService awsImageService;
    @Autowired
    private LogTableRepository logRepo;

    private static final Logger logger = LoggerFactory.getLogger(FlushImageCommand.class);



    public void execute(){
        if(canExecute()){
            doExecute();
        }
        throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }

    protected Boolean canExecute(){
        if(!Costants.ORIGINAL_IMAGE_TYPE.equalsIgnoreCase(this.dto.getImageType()) &&
                !ImageUtilities.checkStyleAvailability(this.dto.getImageType())){
            String message = "The imageType " + this.dto.getImageType() + " is not available.";
            logger.info(message);
            logRepo.save(new LogTable(LocalDateTime.now().toString(), Costants.LOG_INFO, message));
            return false;
        }
        //Add other checks here
        return true;
    }

    protected void doExecute() {
        if(Costants.ORIGINAL_IMAGE_TYPE.equalsIgnoreCase(this.dto.getImageType())){
            for(ImageTypeEnum type : ImageTypeEnum.values()){
                awsImageService.flushImage(type.getName(), this.dto.getImageName());
            }
        } else {
            awsImageService.flushImage(this.dto.getImageType(), this.dto.getImageName());
        }
    }
}
