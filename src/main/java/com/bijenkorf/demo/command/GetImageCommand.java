package com.bijenkorf.demo.command;

import com.bijenkorf.demo.dto.ImageDto;
import com.bijenkorf.demo.log.LogTable;
import com.bijenkorf.demo.log.LogTableRepository;
import com.bijenkorf.demo.resource.GetImageResource;
import com.bijenkorf.demo.service.ImageService;
import com.bijenkorf.demo.utilities.Costants;
import com.bijenkorf.demo.utilities.ImageUtilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDateTime;

@Component
@Scope("prototype")
public class GetImageCommand {

    private ImageDto dto;

    public GetImageCommand(ImageDto dto) {
        this.dto = dto;
    }



    @Autowired
    private AwsImageService awsImageService;
    @Autowired
    private ImageService imageService;
    @Autowired
    private LogTableRepository logRepo;

    private static final Logger logger = LoggerFactory.getLogger(GetImageCommand.class);



    public GetImageResource execute(){
        if(canExecute()){
            return doExecute();
        }
        throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }

    protected Boolean canExecute(){
        if(!ImageUtilities.checkStyleAvailability(this.dto.getImageType())){
            String message = "The imageType " + this.dto.getImageType() + " is not available.";
            logger.info(message);
            logRepo.save(new LogTable(LocalDateTime.now().toString(), Costants.LOG_INFO, message));
        }
        //Add other checks here
        return true;
    }

    protected GetImageResource doExecute(){
        byte[] byteImage = new byte[0];

        byteImage = awsImageService.retrieveImage(this.dto.getImageType(), this.dto.getImageName());
        if(byteImage.length == 0){
            byteImage = awsImageService.retrieveImage(Costants.ORIGINAL_IMAGE_TYPE, this.dto.getImageName());
            if(byteImage.length == 0){
                byteImage = imageService.retrieveOriginalImage(this.dto.getImageName());
                awsImageService.saveImage(Costants.ORIGINAL_IMAGE_TYPE, this.dto.getImageName(), byteImage);
            }
            byteImage = ImageUtilities.optimizer(byteImage, this.dto.getImageType());
            awsImageService.saveImage(this.dto.getImageType(), this.dto.getImageName(), byteImage);
        }

        //Building of the response object
        GetImageResource resource = new GetImageResource();
        resource.setName(this.dto.getImageName());
        resource.setImageType(this.dto.getImageType());
        resource.setImage(byteImage);

        return resource;
    }

}
