package com.bijenkorf.demo.dto;

public class ImageDto {

    private String imageName;
    private String imageType;
    private String dummySeoName;

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getImageType() {
        return imageType;
    }

    public void setImageType(String imageType) {
        this.imageType = imageType;
    }

    public String getDummySeoName() {
        return dummySeoName;
    }

    public void setDummySeoName(String dummySeoName) {
        this.dummySeoName = dummySeoName;
    }
}
