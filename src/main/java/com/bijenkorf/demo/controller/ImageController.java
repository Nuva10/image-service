package com.bijenkorf.demo.controller;

import com.bijenkorf.demo.command.FlushImageCommand;
import com.bijenkorf.demo.command.GetImageCommand;
import com.bijenkorf.demo.dto.ImageDto;
import com.bijenkorf.demo.resource.GetImageResource;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/image")
public class ImageController {

    @Autowired
    private BeanFactory beanFactory;



    @GetMapping("/show/{imageType}")
    @ResponseBody
    public ResponseEntity<GetImageResource> getImage(@PathVariable("imageType") String imageType,
                                                     @RequestParam(name = "reference") String imageName){

        ImageDto dto = new ImageDto();
        dto.setImageName(imageName);
        dto.setImageType(imageType);

        GetImageCommand command = beanFactory.getBean(GetImageCommand.class,  dto);

        try {
            GetImageResource resource = command.execute();
            return new ResponseEntity<>(resource, HttpStatus.OK);
        } catch (ResponseStatusException e){
            return new ResponseEntity<>(new GetImageResource(), e.getStatus());
        }
    }


    @GetMapping("/show/{imageType}/{dummySeoName}")
    @ResponseBody
    public ResponseEntity<GetImageResource> getImageSeo(@PathVariable("imageType") String imageType,
                                                        @PathVariable("dummySeoName") String dummySeoName,
                                                        @RequestParam(name = "reference") String imageName){

        ImageDto dto = new ImageDto();
        dto.setImageName(imageName);
        dto.setImageType(imageType);
        dto.setDummySeoName(dummySeoName);

        GetImageCommand command = beanFactory.getBean(GetImageCommand.class,  dto);

        try {
            GetImageResource resource = command.execute();
            return new ResponseEntity<>(resource, HttpStatus.OK);
        } catch (ResponseStatusException e){
            return new ResponseEntity<>(new GetImageResource(), e.getStatus());
        }
    }


    @GetMapping("/flush/{imageType}")
    public ResponseEntity<Void> flushImage(@PathVariable("imageType") String imageType,
                                                @RequestParam(name = "reference") String imageName){

        ImageDto dto = new ImageDto();
        dto.setImageName(imageName);
        dto.setImageType(imageType);

        FlushImageCommand command = beanFactory.getBean(FlushImageCommand.class,  dto);

        try {
            command.execute();
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (ResponseStatusException e){
            return new ResponseEntity<>(e.getStatus());
        }
    }
}
